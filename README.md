# Agilite Scrum

 .merge_file_InK7cX
Dev Team :

# Qu'est-ce qu'une équipe de développement Scrum? 

##   En savoir plus sur le rôle de l'équipe de développement Scrum 

  Comme décrit dans le [Guide Scrum](https://translate.googleusercontent.com/translate_c?depth=1&hl=fr&prev=search&rurl=translate.google.com&sl=en&sp=nmt4&u=https://www.scrumguides.org/&xid=17259,15700002,15700021,15700186,15700191,15700256,15700259,15700262&usg=ALkJrhhJpRZrFFqseJ4hS9VZDD3POTTb0A)  , une équipe de développement Scrum est composée de professionnels qui  s’emploient à fournir un produit incrémentable, potentiellement  libérable, du produit «Fait» à la fin de chaque sprint.  Un incrément "Terminé" est requis lors de la révision du sprint.  Seuls les membres de l'équipe de développement créent l'incrément. 

  Les équipes de développement sont structurées et habilitées par l'organisation à organiser et gérer son propre travail.  La synergie qui en résulte optimise l'efficacité globale de l'équipe de développement. 

![Backlog de Sprint](https://scrumorg-website-prod.s3.amazonaws.com/drupal/inline-images/2017-03/SprintBacklog_0.png)


#  What is a Product Owner?

## Learn About the Role of the Product Owner

As described in the [Scrum Guide](https://www.scrumguides.org),  a Scrum Product Owner is responsible for maximizing the value of the  product resulting from the work of the Development Team. How this is  done may vary widely across organizations, Scrum Teams, and individuals.

The Product Owner is the sole person responsible for managing the Product Backlog. Product Backlog management includes:

![Product Backlog](https://scrumorg-website-prod.s3.amazonaws.com/drupal/inline-images/2017-03/SprintBacklog.png)                           Product Backlog

- Clearly expressing Product Backlog items.
- Ordering the items in the Product Backlog to best achieve goals and missions.
- Optimizing the value of the work the Development Team performs.
- Ensuring that the Product Backlog is visible, transparent, and clear to all, and shows what the Scrum Team will work on next.
- Ensuring the Development Team understands items in the Product Backlog to the level needed.

The Product Owner may do the above work, or have the Development Team do it. However, the Product Owner remains accountable.

The Product Owner is one person, not a committee. The Product Owner  may represent the desires of a committee in the Product Backlog, but  those wanting to change a Product Backlog item’s priority must address  the Product Owner.

For the Product Owner to succeed, the entire organization must  respect his or her decisions. The Product Owner’s decisions are visible  in the content and ordering of the Product Backlog. No one can force the  Development Team to work from a different set of requirements.

### Book Now Available

[![PSPO Book](https://scrumorg-website-prod.s3.amazonaws.com/drupal/inline-images/2018-07/Professional%20Scrum%20Product%20Owner%20Book.jpg)](https://www.amazon.com/Professional-Product-Owner-Leveraging-Competitive/dp/0134686470/ref=sr_1_2?ie=UTF8&qid=1531253193&sr=8-2&keywords=professional+scrum+product+owner)

## Product Owner Training

The [Professional Scrum Product Owner](https://www.scrum.org/courses/professional-scrum-product-owner-training)  (PSPO) course is a 2-day course on how to maximize the value of  software products and systems. Product Owners need to have a  concrete understanding of everything that drives value from their  products where students learn through instruction and team-based  exercises. 

## Product Owner Certification

Scrum.org provides 2 levels of Professional Scrum Product Owner certification.  

- [PSPO I](https://www.scrum.org/professional-scrum-product-owner-i-certification)  demonstrates an intermediate understanding of the Scrum Framework and  how to apply it to maximize the value delivered with a product.
- [PSPO II](https://www.scrum.org/professional-scrum-certifications/professional-scrum-product-owner-ii-assessment) demonstrates  an advanced level of understanding of how the Scrum framework can  support the creation of value, and how to achieve it in the real world.

## Product Owner Learning Path

Already a Product Owner or ready to start your journey?  Visit our [Product Owner Learning Path](https://www.scrum.org/pathway/product-owner-learning-path) to take a guided tour through suggested resources for continued learning for Scrum Masters.

.merge_file_1RjH8W